//Scanner is used for the user input such as the users name 
import java.util.Scanner;


public class Restaurant_Booking_System {
	
	public static void main(String[] args) {
		Scanner user_input = new Scanner(System.in);
		Scanner scanner = new Scanner(System.in);
		
		//This stores each individual seat number
		String seat1;
		seat1 = "[ 0 1 ]";
		String seat2;
		seat2 = "[ 0 2 ]";
		String seat3;
		seat3 = "[ 0 3 ]";
		String seat4;
		seat4 = "[ 0 4 ]";
		String seat5;
		seat5 = "[ 0 5 ]";
		String seat6;
		seat6 = "[ 0 6 ]";
		String seat7;
		seat7 = "[ 0 7 ]";
		String seat8;
		seat8 = "[ 0 8 ]";
		String seat9;
		seat9 = "[ 0 9 ]";
		String seat10;
		seat10 = "[ 1 0 ]";
		String seat11;
		seat11 = "[ 1 1 ]";
		String seat12;
		seat12 = "[ 1 2 ]";
		String seat13;
		seat13 = "[ 1 3 ]";
		String seat14;
		seat14 = "[ 1 4 ]";
		String seat15;
		seat15 = "[ 1 5 ]";
		String seat16;
		seat16 = "[ 1 6 ]";
		String seat17;
		seat17 = "[ 1 7 ]";
		String seat18;
		seat18 = "[ 1 8 ]";
		String seat19;
		seat19 = "[ 1 9 ]";
		String seat20;
		seat20 = "[ 2 0 ]";
		String seat21;
		seat21 = "[ 2 1 ]";
		String seat22;
		seat22 = "[ 2 2 ]";
		String seat23;
		seat23 = "[ 2 3 ]";
		String seat24;
		seat24 = "[ 2 4 ]";
		String seat25;
		seat25 = "[ 2 5 ]";
		String seat26;
		seat26 = "[ 2 6 ]";
		String seat27;
		seat27 = "[ 2 7 ]";
		String seat28;
		seat28 = "[ 2 8 ]";
		String seat29;
		seat29 = "[ 2 9 ]";
		String seat30;
		seat30 = "[ 3 0 ]";
		String seat31;
		seat31 = "[ 3 1 ]";
		String seat32;
		seat32 = "[ 3 2 ]";
		String seat33;
		seat33 = "[ 3 3 ]";
		String seat34;
		seat34 = "[ 3 4 ]";
		String seat35;
		seat35 = "[ 3 5 ]";
		String seat36;
		seat36 = "[ 3 6 ]";
		String seat37;
		seat37 = "[ 3 7 ]";
		String seat38;
		seat38 = "[ 3 8 ]";
		String seat39;
		seat39 = "[ 3 9 ]";
		String seat40;
		seat40 = "[ 4 0 ]";
		String seat41;
		seat41 = "[ 4 1 ]";
		String seat42;
		seat42 = "[ 4 2 ]";
		
		//This is used to change the selected seat to xx
		String seat_selected;
		seat_selected = "[ x x ]";
		
		//This stores the menu
		String menu;
		menu = "Code Item       Price Quantity\r\n"
				+ "01   Pizza      �10   50\r\n"
				+ "02   Steak      �7    50\r\n"
				+ "03   Sandwich   �5    50\r\n"
				+ "04   Water      �1    30\r\n"
				+ "05   Soft drink �2    30\r\n"
				+ "06   Tea        �2    20\r\n"
				+ "07   Coffee     �2    20\r\n"
				+ "08   Ice cream  �2    20\r\n"
				+ "09   Chocolate  �2    20";
		
		//This asks for the users first name and stores it 
		String first_name;
	    System.out.println("Please type your first name: ");
	    first_name = user_input.next( );
	    
	  //This asks for the users last name and stores it 
	    String last_name;
	    System.out.println("Please type your last name: ");
	    //This is used to store the users last name 
	    last_name = user_input.next( );
	    
	    //This displays the seats in order and asks the user to input a seat number 
	    String Seat_number;
	    System.out.println(seat1 + seat2 + seat3 + seat4 + seat5 + seat6 + seat7 + "\r\n"
						+ seat8 + seat9 + seat10 + seat11 + seat12 + seat13 + seat14 + "\r\n"
						+ seat15 + seat16 + seat17 + seat18 + seat19 + seat20 + seat21 + "\r\n"
						+ seat22 + seat23 + seat24 + seat25 + seat26 + seat27 + seat28 + "\r\n"
						+ seat29 + seat30 + seat31 + seat32 + seat33 + seat34 + seat35 + "\r\n"
						+ seat36 + seat37 + seat38 + seat39 + seat40 + seat41 + seat42 + "\r\n"
						+ "\r\nPlease select a seat number 1-42: ");
	    				Seat_number = scanner.next();
	 /*This check if the seat selected is available
	  *If it is available it makes that seat unavilable for the next person 
	  */ 
	    if (user_input.equals(1)) {
	    	if (seat1 == seat_selected) {
	    		System.out.println("Sorry that seat is already selected. Please choose another one");
	    		user_input.equals(0);
	    	}else {
	    	seat1 = seat_selected;
	    	Seat_number = "01";
	    	}
	    	}else if (user_input.equals(2)) {
	    		if (seat3 == seat_selected) {
		    		System.out.println("Sorry that seat is already selected. Please choose another one");
		    		user_input.equals(0);
		    	}else {
	    		seat2 = seat_selected;
	    		Seat_number = "02";
		    	}
	    	}else if (user_input.equals(3)) {
	    		if (seat3 == seat_selected) {
		    		System.out.println("Sorry that seat is already selected. Please choose another one");
		    		user_input.equals(0);
		    	}else {
	    		seat3 = seat_selected;
	    		Seat_number = "03";
		    	}
	    	}else if (user_input.equals(4)) {
		    		if (seat4 == seat_selected) {
			    		System.out.println("Sorry that seat is already selected. Please choose another one");
			    		user_input.equals(0);
			    	}else {
		    		seat4 = seat_selected;
		    		Seat_number = "04";
			    	}
	    	}else if (user_input.equals(5)) {
	    		if (seat5 == seat_selected) {
		    		System.out.println("Sorry that seat is already selected. Please choose another one");
		    		user_input.equals(0);
		    	}else {
	    		seat5 = seat_selected;
	    		Seat_number = "05";
		    	}
	    	}else if (user_input.equals(6)) {
	    		if (seat6 == seat_selected) {
		    		System.out.println("Sorry that seat is already selected. Please choose another one");
		    		user_input.equals(0);
		    	}else {
	    		seat6 = seat_selected;
	    		Seat_number = "06";
		    	}
	    	}else if (user_input.equals(7)) {
	    		if (seat7 == seat_selected) {
		    		System.out.println("Sorry that seat is already selected. Please choose another one");
		    		user_input.equals(0);
		    	}else {
	    		seat7 = seat_selected;
	    		Seat_number = "07";
		    	}
	    	}else if (user_input.equals(8)) {
	    		if (seat8 == seat_selected) {
		    		System.out.println("Sorry that seat is already selected. Please choose another one");
		    		user_input.equals(0);
		    	}else {
	    		seat3 = seat_selected;
	    		Seat_number = "08";
		    	}
	    	}else if (user_input.equals(9)) {
	    		if (seat9 == seat_selected) {
		    		System.out.println("Sorry that seat is already selected. Please choose another one");
		    		user_input.equals(0);
		    	}else {
	    		seat9 = seat_selected;
	    		Seat_number = "09";
		    	}
	    	}else if (user_input.equals(10)) {
	    		if (seat10 == seat_selected) {
		    		System.out.println("Sorry that seat is already selected. Please choose another one");
		    		user_input.equals(0);
		    	}else {
	    		seat10 = seat_selected;
	    		Seat_number = "10";
		    	}
	    	}else if (user_input.equals(11)) {
	    		if (seat11 == seat_selected) {
		    		System.out.println("Sorry that seat is already selected. Please choose another one");
		    		user_input.equals(0);
		    	}else {
	    		seat11 = seat_selected;
	    		Seat_number = "11";
		    	}
	    	}else if (user_input.equals(12)) {
	    		if (seat12 == seat_selected) {
		    		System.out.println("Sorry that seat is already selected. Please choose another one");
		    		user_input.equals(0);
		    	}else {
	    		seat12 = seat_selected;
	    		Seat_number = "12";
		    	}
	    	}else if (user_input.equals(13)) {
	    		if (seat13 == seat_selected) {
		    		System.out.println("Sorry that seat is already selected. Please choose another one");
		    		user_input.equals(0);
		    	}else {
	    		seat13 = seat_selected;
	    		Seat_number = "13";
		    	}
	    	}else if (user_input.equals(14)) {
	    		if (seat14 == seat_selected) {
		    		System.out.println("Sorry that seat is already selected. Please choose another one");
		    		user_input.equals(0);
		    	}else {
	    		seat14 = seat_selected;
	    		Seat_number = "14";
		    	}
	    	}else if (user_input.equals(15)) {
	    		if (seat15 == seat_selected) {
		    		System.out.println("Sorry that seat is already selected. Please choose another one");
		    		user_input.equals(0);
		    	}else {
	    		seat15 = seat_selected;
	    		Seat_number = "15";
		    	}
	    	}else if (user_input.equals(16)) {
	    		if (seat16 == seat_selected) {
		    		System.out.println("Sorry that seat is already selected. Please choose another one");
		    		user_input.equals(0);
		    	}else {
	    		seat16 = seat_selected;
	    		Seat_number = "16";
		    	}
	    	}else if (user_input.equals(17)) {
	    		if (seat17 == seat_selected) {
		    		System.out.println("Sorry that seat is already selected. Please choose another one");
		    		user_input.equals(0);
		    	}else {
	    		seat17 = seat_selected;
	    		Seat_number = "17";
		    	}
	    	}else if (user_input.equals(18)) {
	    		if (seat18 == seat_selected) {
		    		System.out.println("Sorry that seat is already selected. Please choose another one");
		    		user_input.equals(0);
		    	}else {
	    		seat18 = seat_selected;
	    		Seat_number = "18";
		    	}
	    	}else if (user_input.equals(19)) {
	    		if (seat19 == seat_selected) {
		    		System.out.println("Sorry that seat is already selected. Please choose another one");
		    		user_input.equals(0);
		    	}else {
	    		seat19 = seat_selected;
	    		Seat_number = "19";
		    	}
	    	}else if (user_input.equals(20)) {
	    		if (seat20 == seat_selected) {
		    		System.out.println("Sorry that seat is already selected. Please choose another one");
		    		user_input.equals(0);
		    	}else {
	    		seat20 = seat_selected;
	    		Seat_number = "20";
		    	}
	    	}else if (user_input.equals(21)) {
	    		if (seat21 == seat_selected) {
		    		System.out.println("Sorry that seat is already selected. Please choose another one");
		    		user_input.equals(0);
		    	}else {
	    		seat21 = seat_selected;
	    		Seat_number = "21";
		    	}
	    	}else if (user_input.equals(22)) {
	    		if (seat22 == seat_selected) {
		    		System.out.println("Sorry that seat is already selected. Please choose another one");
		    		user_input.equals(0);
		    	}else {
	    		seat22 = seat_selected;
	    		Seat_number = "22";
		    	}
	    	}else if (user_input.equals(23)) {
	    		if (seat23 == seat_selected) {
		    		System.out.println("Sorry that seat is already selected. Please choose another one");
		    		user_input.equals(0);
		    	}else {
	    		seat23 = seat_selected;
	    		Seat_number = "23";
		    	}
	    	}else if (user_input.equals(24)) {
	    		if (seat24 == seat_selected) {
		    		System.out.println("Sorry that seat is already selected. Please choose another one");
		    		user_input.equals(0);
		    	}else {
	    		seat24 = seat_selected;
	    		Seat_number = "24";
		    	}
	    	}else if (user_input.equals(25)) {
	    		if (seat25 == seat_selected) {
		    		System.out.println("Sorry that seat is already selected. Please choose another one");
		    		user_input.equals(0);
		    	}else {
	    		seat25 = seat_selected;
	    		Seat_number = "25";
		    	}
	    	}else if (user_input.equals(26)) {
	    		if (seat26 == seat_selected) {
		    		System.out.println("Sorry that seat is already selected. Please choose another one");
		    		user_input.equals(0);
		    	}else {
	    		seat26 = seat_selected;
	    		Seat_number = "26";
		    	}
	    	}else if (user_input.equals(27)) {
	    		if (seat27 == seat_selected) {
		    		System.out.println("Sorry that seat is already selected. Please choose another one");
		    		user_input.equals(0);
		    	}else {
	    		seat27 = seat_selected;
	    		Seat_number = "27";
		    	}
	    	}else if (user_input.equals(28)) {
	    		if (seat28 == seat_selected) {
		    		System.out.println("Sorry that seat is already selected. Please choose another one");
		    		user_input.equals(0);
		    	}else {
	    		seat28 = seat_selected;
	    		Seat_number = "28";
		    	}
	    	}else if (user_input.equals(29)) {
	    		if (seat29 == seat_selected) {
		    		System.out.println("Sorry that seat is already selected. Please choose another one");
		    		user_input.equals(0);
		    	}else {
	    		seat29 = seat_selected;
	    		Seat_number = "29";
		    	}
	    	}else if (user_input.equals(30)) {
	    		if (seat30 == seat_selected) {
		    		System.out.println("Sorry that seat is already selected. Please choose another one");
		    		user_input.equals(0);
		    	}else {
	    		seat30 = seat_selected;
	    		Seat_number = "30";
		    	}
	    	}else if (user_input.equals(31)) {
	    		if (seat31 == seat_selected) {
		    		System.out.println("Sorry that seat is already selected. Please choose another one");
		    		user_input.equals(0);
		    	}else {
	    		seat31 = seat_selected;
	    		Seat_number = "31";
		    	}
	    	}else if (user_input.equals(32)) {
	    		if (seat32 == seat_selected) {
		    		System.out.println("Sorry that seat is already selected. Please choose another one");
		    		user_input.equals(0);
		    	}else {
	    		seat32 = seat_selected;
	    		Seat_number = "32";
		    	}
	    	}else if (user_input.equals(33)) {
	    		if (seat33 == seat_selected) {
		    		System.out.println("Sorry that seat is already selected. Please choose another one");
		    		user_input.equals(0);
		    	}else {
	    		seat33 = seat_selected;
	    		Seat_number = "33";
		    	}
	    	}else if (user_input.equals(34)) {
	    		if (seat34 == seat_selected) {
		    		System.out.println("Sorry that seat is already selected. Please choose another one");
		    		user_input.equals(0);
		    	}else {
	    		seat34 = seat_selected;
	    		Seat_number = "34";
		    	}
	    	}else if (user_input.equals(35)) {
	    		if (seat35 == seat_selected) {
		    		System.out.println("Sorry that seat is already selected. Please choose another one");
		    		user_input.equals(0);
		    	}else {
	    		seat35 = seat_selected;
	    		Seat_number = "35";
		    	}
	    	}else if (user_input.equals(36)) {
	    		if (seat36 == seat_selected) {
		    		System.out.println("Sorry that seat is already selected. Please choose another one");
		    		user_input.equals(0);
		    	}else {
	    		seat36 = seat_selected;
	    		Seat_number = "36";
		    	}
	    	}else if (user_input.equals(37)) {
	    		if (seat37 == seat_selected) {
		    		System.out.println("Sorry that seat is already selected. Please choose another one");
		    		user_input.equals(0);
		    	}else {
	    		seat37 = seat_selected;
	    		Seat_number = "37";
		    	}
	    	}else if (user_input.equals(38)) {
	    		if (seat38 == seat_selected) {
		    		System.out.println("Sorry that seat is already selected. Please choose another one");
		    		user_input.equals(0);
		    	}else {
	    		seat38 = seat_selected;
	    		Seat_number = "38";
		    	}
	    	}else if (user_input.equals(39)) {
	    		if (seat39 == seat_selected) {
		    		System.out.println("Sorry that seat is already selected. Please choose another one");
		    		user_input.equals(0);
		    	}else {
	    		seat39 = seat_selected;
	    		Seat_number = "39";
		    	}
	    	}else if (user_input.equals(40)) {
	    		if (seat40 == seat_selected) {
		    		System.out.println("Sorry that seat is already selected. Please choose another one");
		    		user_input.equals(0);
		    	}else {
	    		seat40 = seat_selected;
	    		Seat_number = "40";
		    	}
	    	}else if (user_input.equals(41)) {
	    		if (seat41 == seat_selected) {
		    		System.out.println("Sorry that seat is already selected. Please choose another one");
		    		user_input.equals(0);
		    	}else {
	    		seat41 = seat_selected;
	    		Seat_number = "41";
		    	}
	    	}else if (user_input.equals(42)) {
	    		if (seat42 == seat_selected) {
		    		System.out.println("Sorry that seat is already selected. Please choose another one");
		    		user_input.equals(0);
		    	}else {
	    		seat42 = seat_selected;
	    		Seat_number = "42";
		    	}
	    	}else if (user_input.equals(43)){
		    		System.out.println("Invaled number use 1 to 42");
		    		user_input.equals(0);
		    	}
	    
	    
	    
	    //This shows the menu and asks the users to place their order
	    String order;
	    System.out.println(menu + "\r\nEnter the price of the item you want: ");
	    order = user_input.next();
	    
	    
	    //This show all the information the users inputed 		
	    String final_order;
	    System.out.println("Name: " + first_name + " " + last_name + "\r\n" + "Seat number: " + Seat_number + "\r\n" + "Price: " + "�" + order );
	    
	    
	  }
}
